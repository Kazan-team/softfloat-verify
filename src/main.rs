// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information

use std::io;
use std::mem;

#[derive(Clone, Debug)]
enum Token {
    Number(u128),
    NewLine,
    Identifier(String),
}

#[derive(Debug)]
struct TokenIter<ReadLine> {
    read_line: ReadLine,
    line_buffer: String,
    line_position: usize,
}

impl<ReadLine: FnMut(&mut String) -> io::Result<()>> TokenIter<ReadLine> {
    fn new(read_line: ReadLine) -> io::Result<Self> {
        Ok(Self {
            read_line,
            line_buffer: String::new(),
            line_position: 0,
        })
    }
}

impl<ReadLine: FnMut(&mut String) -> io::Result<()>> Iterator for TokenIter<ReadLine> {
    type Item = io::Result<Token>;
    fn next(&mut self) -> Option<io::Result<Token>> {
        let mut parse_token = || -> io::Result<Option<Token>> {
            if self.line_buffer.is_empty() {
                self.line_position = 0;
                (self.read_line)(&mut self.line_buffer)?;
                if self.line_buffer.is_empty() {
                    return Ok(None);
                }
            }
            let mut chars = self.line_buffer[self.line_position..]
                .char_indices()
                .peekable();
            let first_char = loop {
                if let Some(&(_, first_char)) = chars.peek() {
                    if first_char.is_whitespace() {
                        chars.next();
                    } else {
                        break first_char;
                    }
                } else {
                    self.line_position = 0;
                    self.line_buffer.clear();
                    return Ok(Some(Token::NewLine));
                }
            };
            let token = if first_char.is_digit(10) {
                let radix = if first_char == '0' {
                    chars.next();
                    if let Some(&(index_offset, second_char)) = chars.peek() {
                        let radix = match second_char {
                            'x' | 'X' => 16,
                            'o' | 'O' => 8,
                            'b' | 'B' => 2,
                            ch if ch.is_ascii_digit() => {
                                return Err(io::Error::new(
                                    io::ErrorKind::InvalidInput,
                                    "C-style octal numbers aren't supported to avoid confusion with decimal. Use 0o123 instead.",
                                ));
                            }
                            _ => {
                                self.line_position += index_offset;
                                return Ok(Some(Token::Number(0)));
                            }
                        };
                        chars.next();
                        radix
                    } else {
                        self.line_position = self.line_buffer.len();
                        return Ok(Some(Token::Number(0)));
                    }
                } else {
                    10
                };
                let mut any_digits = false;
                let mut value = 0u128;
                let max_value_div_radix = u128::max_value() / radix;
                let max_value_rem_radix = (u128::max_value() % radix) as u32;
                while let Some(digit) = chars.peek().and_then(|&(_, ch)| ch.to_digit(radix as u32))
                {
                    any_digits = true;
                    chars.next();
                    if value > max_value_div_radix
                        || (value == max_value_div_radix && digit > max_value_rem_radix)
                    {
                        return Err(io::Error::new(
                            io::ErrorKind::InvalidInput,
                            "number too big",
                        ));
                    }
                    value *= radix;
                    value += u128::from(digit);
                }
                if !any_digits {
                    return Err(io::Error::new(
                        io::ErrorKind::InvalidInput,
                        "number has no digits",
                    ));
                }
                Token::Number(value)
            } else if first_char.is_ascii_alphabetic() || first_char == '_' {
                let mut text = String::new();
                while let Some(ch) = chars
                    .peek()
                    .map(|&(_, ch)| ch)
                    .filter(|&ch| ch.is_ascii_alphanumeric() || ch == '_')
                {
                    chars.next();
                    text.push(ch);
                }
                Token::Identifier(text)
            } else {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "invalid character",
                ));
            };
            self.line_position = if let Some((index, _)) = chars.next() {
                self.line_position + index
            } else {
                self.line_buffer.len()
            };
            Ok(Some(token))
        };
        match parse_token() {
            Ok(v) => Some(Ok(v?)),
            Err(v) => Some(Err(v)),
        }
    }
}

enum Argument {
    Initial(u128),
    F128(softfloat_sys::float128_t),
    F80(softfloat_sys::extFloat80M),
}

impl Argument {
    fn get_initial_value(&self) -> u128 {
        match *self {
            Argument::Initial(v) => v,
            _ => unreachable!("argument is not in the initial state"),
        }
    }
}

trait GetArgument<T> {
    fn get_argument(&mut self) -> T;
}

macro_rules! simple_argument {
    ($t:ident) => {
        impl GetArgument<$t> for Argument {
            fn get_argument(&mut self) -> $t {
                self.get_initial_value() as $t
            }
        }
    };
}

simple_argument!(u8);
simple_argument!(u16);
simple_argument!(u32);
simple_argument!(u64);
simple_argument!(i8);
simple_argument!(i16);
simple_argument!(i32);
simple_argument!(i64);

impl GetArgument<bool> for Argument {
    fn get_argument(&mut self) -> bool {
        self.get_initial_value() != 0
    }
}

impl GetArgument<softfloat_sys::float16_t> for Argument {
    fn get_argument(&mut self) -> softfloat_sys::float16_t {
        softfloat_sys::float16_t {
            v: self.get_initial_value() as u16,
        }
    }
}

impl GetArgument<softfloat_sys::float32_t> for Argument {
    fn get_argument(&mut self) -> softfloat_sys::float32_t {
        softfloat_sys::float32_t {
            v: self.get_initial_value() as u32,
        }
    }
}

impl GetArgument<softfloat_sys::float64_t> for Argument {
    fn get_argument(&mut self) -> softfloat_sys::float64_t {
        softfloat_sys::float64_t {
            v: self.get_initial_value() as u64,
        }
    }
}

impl GetArgument<softfloat_sys::float128_t> for Argument {
    fn get_argument(&mut self) -> softfloat_sys::float128_t {
        softfloat_sys::float128_t {
            v: unsafe { mem::transmute::<u128, [u64; 2]>(self.get_initial_value()) },
        }
    }
}

impl GetArgument<*const softfloat_sys::float128_t> for Argument {
    fn get_argument(&mut self) -> *const softfloat_sys::float128_t {
        *self = Argument::F128(self.get_argument());
        match *self {
            Argument::F128(ref retval) => retval,
            _ => unreachable!(),
        }
    }
}

impl GetArgument<softfloat_sys::extFloat80M> for Argument {
    fn get_argument(&mut self) -> softfloat_sys::extFloat80M {
        let v = self.get_initial_value();
        softfloat_sys::extFloat80M {
            signExp: (v >> 64) as u16,
            signif: v as u64,
        }
    }
}

impl GetArgument<*const softfloat_sys::extFloat80M> for Argument {
    fn get_argument(&mut self) -> *const softfloat_sys::extFloat80M {
        *self = Argument::F80(self.get_argument());
        match *self {
            Argument::F80(ref retval) => retval,
            _ => unreachable!(),
        }
    }
}

trait HandleResult {
    fn handle(result: Self, stack: &mut Vec<u128>);
}

impl HandleResult for () {
    fn handle(_result: Self, _stack: &mut Vec<u128>) {}
}

macro_rules! simple_result {
    ($t:ident, $inter_t:ident) => {
        impl HandleResult for $t {
            fn handle(result: Self, stack: &mut Vec<u128>) {
                #[allow(clippy::cast_lossless)]
                stack.push(u128::from(result as $inter_t));
            }
        }
    };
}

simple_result!(u8, u8);
simple_result!(u16, u16);
simple_result!(u32, u32);
simple_result!(u64, u64);
simple_result!(i8, u8);
simple_result!(i16, u16);
simple_result!(i32, u32);
simple_result!(i64, u64);

impl HandleResult for softfloat_sys::float16_t {
    fn handle(result: Self, stack: &mut Vec<u128>) {
        stack.push(u128::from(result.v));
    }
}

impl HandleResult for softfloat_sys::float32_t {
    fn handle(result: Self, stack: &mut Vec<u128>) {
        stack.push(u128::from(result.v));
    }
}

impl HandleResult for softfloat_sys::float64_t {
    fn handle(result: Self, stack: &mut Vec<u128>) {
        stack.push(u128::from(result.v));
    }
}

impl HandleResult for softfloat_sys::float128_t {
    fn handle(result: Self, stack: &mut Vec<u128>) {
        stack.push(unsafe { mem::transmute::<[u64; 2], u128>(result.v) });
    }
}

impl HandleResult for softfloat_sys::extFloat80M {
    fn handle(result: Self, stack: &mut Vec<u128>) {
        stack.push((u128::from(result.signif) << 64) | u128::from(result.signExp));
    }
}

impl HandleResult for bool {
    fn handle(result: Self, stack: &mut Vec<u128>) {
        stack.push(if result { 1 } else { 0 });
    }
}

#[allow(clippy::cyclomatic_complexity)]
fn handle_fn(name: &str, stack: &mut Vec<u128>) -> io::Result<()> {
    macro_rules! second_arg {
        ($first:tt, $second:tt) => {
            $second
        };
    }
    macro_rules! handle_functions {
        {
            ($name:expr, $stack:expr);
            $(const $constant:ident;)*
            $(fn $function:ident($($argument:ident),*$(; mut $return_argument:ident)*);)*
        } => {
            match $name {
                $(
                    stringify!($constant) => {
                        HandleResult::handle(softfloat_sys::$constant, $stack);
                        return Ok(());
                    }
                )*
                $(
                    stringify!($function) => {
                        let mut arguments = [$(second_arg!($argument, 0)),*];
                        for argument in arguments.iter_mut().rev() {
                            if let Some(v) = stack.pop() {
                                *argument = v;
                            } else {
                                return Err(io::Error::new(
                                    io::ErrorKind::Other,
                                    concat!("not enough arguments for ", stringify!($function)),
                                ));
                            }
                        }
                        #[allow(unused_mut, unused_variables)]
                        let mut arguments = arguments.iter();
                        $(let mut $argument = Argument::Initial(*arguments.next().unwrap());)*
                        unsafe {
                            $(let mut $return_argument = mem::zeroed();)*
                            let result = softfloat_sys::$function($($argument.get_argument()),*$(, &mut $return_argument)*);
                            $(HandleResult::handle($return_argument, $stack);)*
                            HandleResult::handle(result, $stack);
                        }
                        return Ok(());
                    }
                )*
                _ => {}
            }
        };
    }

    handle_functions! {
        (name, stack);
        const softfloat_tininess_beforeRounding;
        const softfloat_tininess_afterRounding;
        const softfloat_round_near_even;
        const softfloat_round_minMag;
        const softfloat_round_min;
        const softfloat_round_max;
        const softfloat_round_near_maxMag;
        const softfloat_round_odd;
        const softfloat_flag_inexact;
        const softfloat_flag_underflow;
        const softfloat_flag_overflow;
        const softfloat_flag_infinite;
        const softfloat_flag_invalid;
        fn softfloat_detectTininess_read_helper();
        fn softfloat_detectTininess_write_helper(v);
        fn softfloat_roundingMode_read_helper();
        fn softfloat_roundingMode_write_helper(v);
        fn softfloat_exceptionFlags_read_helper();
        fn softfloat_exceptionFlags_write_helper(v);
        fn extF80_roundingPrecision_read_helper();
        fn extF80_roundingPrecision_write_helper(v);
        fn softfloat_raiseFlags(v0);
        fn ui32_to_f16(v0);
        fn ui32_to_f32(v0);
        fn ui32_to_f64(v0);
        fn ui32_to_extF80(v0);
        fn ui32_to_f128(v0);
        fn ui32_to_extF80M(v0; mut v1);
        fn ui32_to_f128M(v0; mut v1);
        fn ui64_to_f16(v0);
        fn ui64_to_f32(v0);
        fn ui64_to_f64(v0);
        fn ui64_to_extF80(v0);
        fn ui64_to_f128(v0);
        fn ui64_to_extF80M(v0; mut v1);
        fn ui64_to_f128M(v0; mut v1);
        fn i32_to_f16(v0);
        fn i32_to_f32(v0);
        fn i32_to_f64(v0);
        fn i32_to_extF80(v0);
        fn i32_to_f128(v0);
        fn i32_to_extF80M(v0; mut v1);
        fn i32_to_f128M(v0; mut v1);
        fn i64_to_f16(v0);
        fn i64_to_f32(v0);
        fn i64_to_f64(v0);
        fn i64_to_extF80(v0);
        fn i64_to_f128(v0);
        fn i64_to_extF80M(v0; mut v1);
        fn i64_to_f128M(v0; mut v1);
        fn f16_to_ui32(v0, v1, v2);
        fn f16_to_ui64(v0, v1, v2);
        fn f16_to_i32(v0, v1, v2);
        fn f16_to_i64(v0, v1, v2);
        fn f16_to_ui32_r_minMag(v0, v1);
        fn f16_to_ui64_r_minMag(v0, v1);
        fn f16_to_i32_r_minMag(v0, v1);
        fn f16_to_i64_r_minMag(v0, v1);
        fn f16_to_f32(v0);
        fn f16_to_f64(v0);
        fn f16_to_extF80(v0);
        fn f16_to_f128(v0);
        fn f16_to_extF80M(v0; mut v1);
        fn f16_to_f128M(v0; mut v1);
        fn f16_roundToInt(v0, v1, v2);
        fn f16_add(v0, v1);
        fn f16_sub(v0, v1);
        fn f16_mul(v0, v1);
        fn f16_mulAdd(v0, v1, v2);
        fn f16_div(v0, v1);
        fn f16_rem(v0, v1);
        fn f16_sqrt(v0);
        fn f16_eq(v0, v1);
        fn f16_le(v0, v1);
        fn f16_lt(v0, v1);
        fn f16_eq_signaling(v0, v1);
        fn f16_le_quiet(v0, v1);
        fn f16_lt_quiet(v0, v1);
        fn f16_isSignalingNaN(v0);
        fn f32_to_ui32(v0, v1, v2);
        fn f32_to_ui64(v0, v1, v2);
        fn f32_to_i32(v0, v1, v2);
        fn f32_to_i64(v0, v1, v2);
        fn f32_to_ui32_r_minMag(v0, v1);
        fn f32_to_ui64_r_minMag(v0, v1);
        fn f32_to_i32_r_minMag(v0, v1);
        fn f32_to_i64_r_minMag(v0, v1);
        fn f32_to_f16(v0);
        fn f32_to_f64(v0);
        fn f32_to_extF80(v0);
        fn f32_to_f128(v0);
        fn f32_to_extF80M(v0; mut v1);
        fn f32_to_f128M(v0; mut v1);
        fn f32_roundToInt(v0, v1, v2);
        fn f32_add(v0, v1);
        fn f32_sub(v0, v1);
        fn f32_mul(v0, v1);
        fn f32_mulAdd(v0, v1, v2);
        fn f32_div(v0, v1);
        fn f32_rem(v0, v1);
        fn f32_sqrt(v0);
        fn f32_eq(v0, v1);
        fn f32_le(v0, v1);
        fn f32_lt(v0, v1);
        fn f32_eq_signaling(v0, v1);
        fn f32_le_quiet(v0, v1);
        fn f32_lt_quiet(v0, v1);
        fn f32_isSignalingNaN(v0);
        fn f64_to_ui32(v0, v1, v2);
        fn f64_to_ui64(v0, v1, v2);
        fn f64_to_i32(v0, v1, v2);
        fn f64_to_i64(v0, v1, v2);
        fn f64_to_ui32_r_minMag(v0, v1);
        fn f64_to_ui64_r_minMag(v0, v1);
        fn f64_to_i32_r_minMag(v0, v1);
        fn f64_to_i64_r_minMag(v0, v1);
        fn f64_to_f16(v0);
        fn f64_to_f32(v0);
        fn f64_to_extF80(v0);
        fn f64_to_f128(v0);
        fn f64_to_extF80M(v0; mut v1);
        fn f64_to_f128M(v0; mut v1);
        fn f64_roundToInt(v0, v1, v2);
        fn f64_add(v0, v1);
        fn f64_sub(v0, v1);
        fn f64_mul(v0, v1);
        fn f64_mulAdd(v0, v1, v2);
        fn f64_div(v0, v1);
        fn f64_rem(v0, v1);
        fn f64_sqrt(v0);
        fn f64_eq(v0, v1);
        fn f64_le(v0, v1);
        fn f64_lt(v0, v1);
        fn f64_eq_signaling(v0, v1);
        fn f64_le_quiet(v0, v1);
        fn f64_lt_quiet(v0, v1);
        fn f64_isSignalingNaN(v0);
        fn extF80_to_ui32(v0, v1, v2);
        fn extF80_to_ui64(v0, v1, v2);
        fn extF80_to_i32(v0, v1, v2);
        fn extF80_to_i64(v0, v1, v2);
        fn extF80_to_ui32_r_minMag(v0, v1);
        fn extF80_to_ui64_r_minMag(v0, v1);
        fn extF80_to_i32_r_minMag(v0, v1);
        fn extF80_to_i64_r_minMag(v0, v1);
        fn extF80_to_f16(v0);
        fn extF80_to_f32(v0);
        fn extF80_to_f64(v0);
        fn extF80_to_f128(v0);
        fn extF80_roundToInt(v0, v1, v2);
        fn extF80_add(v0, v1);
        fn extF80_sub(v0, v1);
        fn extF80_mul(v0, v1);
        fn extF80_div(v0, v1);
        fn extF80_rem(v0, v1);
        fn extF80_sqrt(v0);
        fn extF80_eq(v0, v1);
        fn extF80_le(v0, v1);
        fn extF80_lt(v0, v1);
        fn extF80_eq_signaling(v0, v1);
        fn extF80_le_quiet(v0, v1);
        fn extF80_lt_quiet(v0, v1);
        fn extF80_isSignalingNaN(v0);
        fn extF80M_to_ui32(v0, v1, v2);
        fn extF80M_to_ui64(v0, v1, v2);
        fn extF80M_to_i32(v0, v1, v2);
        fn extF80M_to_i64(v0, v1, v2);
        fn extF80M_to_ui32_r_minMag(v0, v1);
        fn extF80M_to_ui64_r_minMag(v0, v1);
        fn extF80M_to_i32_r_minMag(v0, v1);
        fn extF80M_to_i64_r_minMag(v0, v1);
        fn extF80M_to_f16(v0);
        fn extF80M_to_f32(v0);
        fn extF80M_to_f64(v0);
        fn extF80M_to_f128M(v0; mut v1);
        fn extF80M_roundToInt(v0, v1, v2; mut v3);
        fn extF80M_add(v0, v1; mut v2);
        fn extF80M_sub(v0, v1; mut v2);
        fn extF80M_mul(v0, v1; mut v2);
        fn extF80M_div(v0, v1; mut v2);
        fn extF80M_rem(v0, v1; mut v2);
        fn extF80M_sqrt(v0; mut v1);
        fn extF80M_eq(v0, v1);
        fn extF80M_le(v0, v1);
        fn extF80M_lt(v0, v1);
        fn extF80M_eq_signaling(v0, v1);
        fn extF80M_le_quiet(v0, v1);
        fn extF80M_lt_quiet(v0, v1);
        fn extF80M_isSignalingNaN(v0);
        fn f128_to_ui32(v0, v1, v2);
        fn f128_to_ui64(v0, v1, v2);
        fn f128_to_i32(v0, v1, v2);
        fn f128_to_i64(v0, v1, v2);
        fn f128_to_ui32_r_minMag(v0, v1);
        fn f128_to_ui64_r_minMag(v0, v1);
        fn f128_to_i32_r_minMag(v0, v1);
        fn f128_to_i64_r_minMag(v0, v1);
        fn f128_to_f16(v0);
        fn f128_to_f32(v0);
        fn f128_to_f64(v0);
        fn f128_to_extF80(v0);
        fn f128_roundToInt(v0, v1, v2);
        fn f128_add(v0, v1);
        fn f128_sub(v0, v1);
        fn f128_mul(v0, v1);
        fn f128_mulAdd(v0, v1, v2);
        fn f128_div(v0, v1);
        fn f128_rem(v0, v1);
        fn f128_sqrt(v0);
        fn f128_eq(v0, v1);
        fn f128_le(v0, v1);
        fn f128_lt(v0, v1);
        fn f128_eq_signaling(v0, v1);
        fn f128_le_quiet(v0, v1);
        fn f128_lt_quiet(v0, v1);
        fn f128_isSignalingNaN(v0);
        fn f128M_to_ui32(v0, v1, v2);
        fn f128M_to_ui64(v0, v1, v2);
        fn f128M_to_i32(v0, v1, v2);
        fn f128M_to_i64(v0, v1, v2);
        fn f128M_to_ui32_r_minMag(v0, v1);
        fn f128M_to_ui64_r_minMag(v0, v1);
        fn f128M_to_i32_r_minMag(v0, v1);
        fn f128M_to_i64_r_minMag(v0, v1);
        fn f128M_to_f16(v0);
        fn f128M_to_f32(v0);
        fn f128M_to_f64(v0);
        fn f128M_to_extF80M(v0; mut v1);
        fn f128M_roundToInt(v0, v1, v2; mut v3);
        fn f128M_add(v0, v1; mut v2);
        fn f128M_sub(v0, v1; mut v2);
        fn f128M_mul(v0, v1; mut v2);
        fn f128M_mulAdd(v0, v1, v2; mut v3);
        fn f128M_div(v0, v1; mut v2);
        fn f128M_rem(v0, v1; mut v2);
        fn f128M_sqrt(v0; mut v1);
        fn f128M_eq(v0, v1);
        fn f128M_le(v0, v1);
        fn f128M_lt(v0, v1);
        fn f128M_eq_signaling(v0, v1);
        fn f128M_le_quiet(v0, v1);
        fn f128M_lt_quiet(v0, v1);
        fn f128M_isSignalingNaN(v0);
    }
    Err(io::Error::new(
        io::ErrorKind::InvalidInput,
        format!("unknown function: {:?}", name),
    ))
}

fn main() -> io::Result<()> {
    let mut stack = Vec::<u128>::new();
    for token in TokenIter::new(|buffer: &mut String| {
        io::stdin().read_line(buffer)?;
        Ok(())
    })? {
        match token? {
            Token::NewLine => {
                let mut iter = stack.drain(..);
                if let Some(v) = iter.next() {
                    print!("{:#x}", v);
                    for v in iter {
                        print!(" {:#x}", v);
                    }
                    println!();
                } else {
                    println!("done.");
                }
            }
            Token::Number(value) => stack.push(value),
            Token::Identifier(name) => handle_fn(&name, &mut stack)?,
        }
    }
    Ok(())
}
